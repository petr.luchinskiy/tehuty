from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.utils.markdown import hcode
from utils.db_api import owner_command as commands
from utils.db_api import counters_command as counters


from keyboards.default import main_menu
from keyboards.default import counter_menu
from loader import dp


@dp.message_handler(text=("Просмотр информации о счетчиках"))
async def counters_start(message: types.Message, state: FSMContext):
    l = await commands.select_all_owners(user_id=int(message.from_user.id))
    if l != []:
        if len(l)>1:
            await message.answer(f'Введите интересующий вас лицевой \nНапоминаю что у вас есть лицевые: \n')
            for i in range(len(l)):
                await message.answer(hcode(f'лицевой номер {l[i].contract_id} и адрес {l[i].address}\n'))
            await state.set_state(("details_counter"))
        if len(l)==1:
            d = await counters.view_counters_detail(contract_id=l[0].contract_id)
            if d != -3:
                for i in range(len(d)):
                    await message.answer(hcode(f'Счетчик с заводским номером: {d[i].factory_numbers} \n'
                                               f'место установки: {d[i].place}\n'
                                               f'дата принятия (год-месяц-день): {d[i].date_accept}\n'
                                               f'тип счетчика: {d[i].type_counters}\n'
                                               f'Дата следующей поверки: {d[i].next_verification_date}\n'
                                               f'Данные актуальны на (год-месяц-день) {d[i].actual}\n'))
            if d == -3:
                await message.answer("нет счетчиков по данному лицевому")
    else:
        await message.answer("не найден ни один лицевой счет")


@dp.message_handler(state="details_counter")
async def enter_email(message: types.Message, state: FSMContext):
    c = message.text
    if c.isdigit() and len(c) <= 6 and c != '0':
        l = await counters.view_counters_detail(contract_id=int(message.text))
        if l != -3:
            for i in range(len(l)):
                await message.answer(hcode(f'Счетчик с заводским номером: {l[i].factory_numbers} \n'
                                           f'место установки: {l[i].place}\n'
                                           f'дата принятия (год-месяц-день): {l[i].date_accept}\n'
                                           f'тип счетчика: {l[i].type_counters}\n'
                                           f'Дата следующей поверки: {l[i].next_verification_date}\n'
                                           f'Данные актуальны на (год-месяц-день) {l[i].actual}\n'))
        if l == -3:
            await message.answer("нет счетчиков по данному лицевому")
    else:
        await message.answer("Не верный лицевой - не более 6 цифр без пробелов")
    await state.finish()
    await message.answer("Выберите что нибудь из меню", reply_markup=main_menu)


@dp.message_handler(text=("Последние показания счетчиков"))
async def counters_record(message: types.Message, state: FSMContext):
    l = await commands.select_all_owners(user_id=int(message.from_user.id))
    if l != []:
        if len(l)>1:
            await message.answer(f'Введите интересующий вас лицевой \nНапоминаю что у вас есть лицевые: \n')
            for i in range(len(l)):
                await message.answer(hcode(f'лицевой номер {l[i].contract_id} и адрес {l[i].address}\n'))
            await state.set_state(("records_counter"))
        if len(l)==1:
            d = await counters.view_records_detail(contract_id=l[0].contract_id)
            if d != -3:
                for i in range(len(d)):
                    await message.answer(hcode(f'Тип Счетчика: {d[i].type_counter} \n'
                                               f'Место установки {d[i].place}\n'
                                               f'Показание: {d[i].value}\n'
                                               f'Дата показания: {d[i].date_record}\n'
                                               f'Данные актуальны на (год-месяц-день) : {d[i].actual}\n'))
            if d == -3:
                await message.answer("нет счетчиков по данному лицевому")
    else:
        await message.answer("не найден ни один лицевой счет")


@dp.message_handler(state="records_counter")
async def view_records_detail(message: types.Message, state: FSMContext):
    c = message.text
    if c.isdigit() and len(c) <= 6 and c != '0':
        l = await counters.view_records_detail(contract_id=int(message.text))
        if l != -3:
            for i in range(len(l)):
                await message.answer(hcode(f'Тип Счетчика: {l[i].type_counter} \n'
                                           f'Место установки {l[i].place}\n'
                                           f'Показание: {l[i].value}\n'
                                           f'Дата показания: {l[i].date_record}\n'
                                           f'Данные актуальны на (год-месяц-день) : {l[i].actual}\n'))
        if l == -3:
            await message.answer("нет счетчиков по данному лицевому")
    else:
        await message.answer("Не верный лицевой - не более 6 цифр без пробелов")
    await state.finish()
    await message.answer("Выберите что нибудь из меню", reply_markup=main_menu)


