from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.utils.markdown import hcode
from utils.db_api import owner_command as commands

from utils.db_api import counters_command as counters

from keyboards.default import main_menu
from loader import dp


@dp.message_handler(text=("Контактная информация"))
async def credit_start(message: types.Message):
    await message.answer(f'Вы можете обратится в водоканал по следующим номерам: \n'
                         f'Главный инженер Кривошеев Сергей Александрович тел. 2-47-37 \n'
                         f'Заместитель директора по экономическим вопросам и финансам\n '
                         f'Воропаева Ирина Юрьевна тел.2-49-47\n'
                         f'Главный бухгалтер Елеусенова Наргуль Серикжановна  тел. 2-49-52\n'
                         f'Главный энергетик Кильдишев Сергей Александрович тел. 2-49-46\n'
                         f'Главный механик  Клименко Сергей Александрович  тел. 2-47-97 \n')



