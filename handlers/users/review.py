from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.builtin import Command
from aiogram.utils.markdown import hcode

from keyboards.default import back_menu
from keyboards.default.main_menu import main_menu

from loader import dp
from utils.db_api import quick_commands as commands


@dp.message_handler(text=("Написать Отзыв"))
async def review_start(message: types.Message, state: FSMContext):
    await  message.answer(("Здесь вы можете написать отзыв или пожелание"), reply_markup=back_menu)
    await state.set_state(("review"))


@dp.message_handler(state="review")
async def enter_review(message: types.Message, state: FSMContext):
    await commands.add_review(text_review=message.text, user_id=message.from_user.id)
    await  message.answer("Спасибо за отзыв", reply_markup=main_menu)
    await state.finish()
