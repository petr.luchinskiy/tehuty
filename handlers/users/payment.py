from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.utils.markdown import hcode
from utils.db_api import owner_command as commands

from utils.db_api import counters_command as counters

from keyboards.default import main_menu
from loader import dp


@dp.message_handler(text=("Тарифы"))
async def credit_start(message: types.Message):
    await message.answer(f'Текущие тарифы водоканала \n'
                                  f'Вы можете открыть pdf файл через браузер \n')
    await message.answer_document(open("tariffs.pdf",'rb'))



