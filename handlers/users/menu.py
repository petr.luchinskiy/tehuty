
import logging

from aiogram import types
from aiogram.dispatcher.filters import Command
from aiogram.types import CallbackQuery

from keyboards.inline.callback_datas import buy_callback
from keyboards.inline.choice_buttons import choice, pear_keyboard
from loader import dp


@dp.message_handler(Command('menu'))
async def show_items(message: types.Message):
    await message.answer(text='выберите услугу '
                              'Если вам ничего не нужно нажмите отмену', reply_markup=choice)



@dp.callback_query_handler(text='cancel')
async def cancel(call: CallbackQuery):
    await call.answer('Вы отменили выбор', show_alert=True)
    await call.message.edit_reply_markup()

