from aiogram import types
from aiogram.dispatcher.filters.builtin import CommandStart
from aiogram.types import ReplyKeyboardRemove
from keyboards.default.counter_menu import counter_menu
from keyboards.default.contract_menu import contract_menu
from keyboards.default.main_menu import main_menu
from loader import dp
from utils.db_api import quick_commands as commands


@dp.message_handler(CommandStart())
async def bot_start(message: types.Message):
    name = message.from_user.full_name
    await  commands.add_user(user_id=message.from_user.id, name=name)

    await message.answer(
        "\n".join(
            [
                f'Привет, {message.from_user.full_name}!, теперь можно работать с Техути',
            ]
        )
    )

    await  message.answer("Выберите услугу", reply_markup=main_menu)


@dp.message_handler(text="Лицевой счет")
async def contract(message: types.Message):
    await message.answer("Здесь вы можете работать с лицевыми счетами", reply_markup=contract_menu)


@dp.message_handler(text="Счетчики")
async def get_counters(message: types.Message):
    await message.answer("В этом разделе вы можете узнать информацию о счетчиках", reply_markup=counter_menu)


@dp.message_handler(text="Начисление")
async def get_credit(message: types.Message):
    await message.answer("Здесь работа с начислением на ваших лицевых счетах", reply_markup=ReplyKeyboardRemove())
