from aiogram.types import CallbackQuery
from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.dispatcher.filters.builtin import Command
from aiogram.utils.markdown import hcode

from loader import dp
from utils.db_api import quick_commands as commands

from keyboards.inline.callback_datas import buy_callback
from loader import dp


@dp.callback_query_handler(buy_callback.filter(item_name='phone'))
async def update_phone(call: CallbackQuery, callback_data: dict, state: FSMContext):
    await call.answer(cache_time=60)
    await call.message.answer(f'Пришлите свой номер')
    await state.set_state("phone")


@dp.message_handler(state="phone")
async def enter_phone(message: types.Message, state: FSMContext):
    e = message.text
    await commands.update_user_phone(phone=e, user_id=message.from_user.id)
    user = await commands.select_user(user_id=message.from_user.id)
    await message.answer("Данные обновлены. \n Ваши данные выглядят: \n" + hcode(f'Имя={user.name}\n'
                                                                              f'телефон={user.phone}'))
    await message.answer("Спасибо, если хотите изменить телефон - выберите еще раз эту услугу")
    await state.finish()
