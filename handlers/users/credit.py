from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.utils.markdown import hcode
from utils.db_api import owner_command as commands

from utils.db_api import counters_command as counters

from keyboards.default import main_menu
from loader import dp


@dp.message_handler(text=("Начисление"))
async def credit_start(message: types.Message, state: FSMContext):
    l = await commands.select_all_owners(user_id=int(message.from_user.id))
    if l != []:
        if len(l) > 1:
            await message.answer(f'Введите интересующий вас лицевой \nНапоминаю что у вас есть лицевые: \n')
            for i in range(len(l)):
                await message.answer(hcode(f'лицевой номер {l[i].contract_id} и адрес {l[i].address}\n'))
            await state.set_state(("details_credit"))
        if len(l)==1:
            d = await commands.view_credit_detail(contract_id=l[0].contract_id)
            if d.flat is None:
                await message.answer(hcode(f'Лицевой №: {d.contract_id} \n'
                                           f'Адрес улица {d.street.strip()} д. {d.house.strip()}\n'
                                           f'Владелец {d.fam.strip()}  {d.name.strip()}\n'
                                           f'Баланс на текущий месяц: {d.hvstek} \n'
                                           f'Полив на текущий месяц:: {d.polivtek} \n'
                                           f'Пеня на текущий месяц:: {d.penyatek} \n'
                                           f'Данные актуальны на (год-месяц-день) {d.actual} \n'))
            else:
                await message.answer(hcode(f'Лицевой №: {d.contract_id} \n'
                                           f'Адрес улица {d.street.strip()} д. {d.house.strip()}\n'
                                           f'Владелец {d.fam.strip()}  {d.name.strip()}\n'
                                           f'Баланс на текущий месяц: {d.hvstek}\n'
                                           f'Полив на текущий месяц:: {d.polivtek}\n'
                                           f'Пеня на текущий месяц:: {d.penyatek}\n'
                                           f'Данные актуальны на (год-месяц-день) {d.actual}\n'))

    else:
        await message.answer("не найден ни один лицевой счет")


@dp.message_handler(state="details_credit")
async def view_c(message: types.Message, state: FSMContext):
    c = message.text
    if c.isdigit() and len(c) <= 6 and c != '0':
        l = await commands.view_credit_detail(contract_id=int(message.text))
        if l != -3:
            if l.flat is None:
                await message.answer(hcode(f'Лицевой №: {l.contract_id} \n'
                                           f'Адрес улица {l.street.strip()} д. {l.house.strip()}\n' 
                                           f'Владелец {l.fam.strip()}  {l.name.strip()}\n'
                                           f'Баланс на текущий месяц: {l.hvstek} \n'
                                           f'Полив на текущий месяц:: {l.polivtek} \n'
                                           f'Пеня на текущий месяц:: {l.penyatek} \n'
                                           f'Данные актуальны на (год-месяц-день) {l.actual} \n'))
            else:
                await message.answer(hcode(f'Лицевой №: {l.contract_id} \n'
                                           f'Адрес улица {l.street.strip()} д. {l.house.strip()}\n' 
                                           f'Владелец {l.fam.strip()}  {l.name.strip()}\n'
                                           f'Баланс на текущий месяц: {l.hvstek}\n'
                                           f'Полив на текущий месяц:: {l.polivtek}\n'
                                           f'Пеня на текущий месяц:: {l.penyatek}\n'
                                           f'Данные актуальны на (год-месяц-день) {l.actual}\n'))

        if l == -3:
            await message.answer("нет счетчиков по данному лицевому", l)
    else:
        await message.answer("Не верный лицевой - не более 6 цифр без пробелов")
    await state.finish()
    await message.answer("Выберите что нибудь из меню", reply_markup=main_menu)
