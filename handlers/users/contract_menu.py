from aiogram import types
from aiogram.dispatcher import FSMContext
from aiogram.utils.markdown import hcode
from utils.db_api import owner_command as commands

from keyboards.default import main_menu
from keyboards.default.contract_menu import contract_menu
from loader import dp


@dp.message_handler(text=("Просмотр лицевых счетов"))
async def select_contract(message: types.Message):
    l = await commands.select_all_owners(user_id=int(message.from_user.id))
    if l != []:
        await message.answer(f'На вас числятся следующие лицевые счета: \n')
        for i in range(len(l)):
            await message.answer(hcode(f'лицевой номер {l[i].contract_id} и адрес {l[i].address}\n'))
    else:
        await message.answer("не найден ни один лицевой счет")


@dp.message_handler(text=("Удалить лицевой счет"))
async def bot_start(message: types.Message, state: FSMContext):
    l = await commands.select_all_owners(user_id=int(message.from_user.id))
    if l != []:
        await message.answer(
            f'На вас числятся следующие лицевые счета: \n Для удаления введите номер лицевого без пробелов')
        for i in range(len(l)):
            await message.answer(hcode(f'лицевой номер {l[i].contract_id} и адрес {l[i].address}\n'))
            await state.set_state(("del_credit"))
    else:
        await message.answer("не найден ни один лицевой счет")


@dp.message_handler(state="del_credit")
async def enter_email(message: types.Message, state: FSMContext):
    c = message.text
    if c.isdigit():
        l = await commands.del_owner(contract_id=int(message.text))
        if l == 0:
            await message.answer('Лицевой был удален успешно', reply_markup=contract_menu)
        if l == -3:
            await message.answer("не верный номер лицевого")
    else:
        await message.answer("в лицевом должны быть только цифры")
    await state.finish()
    await message.answer("Повторите процедуру", reply_markup=contract_menu)


# Добавление лицевого во владельцы
@dp.message_handler(text=("Добавить лицевой счет"))
async def bot_start(message: types.Message, state: FSMContext):
    await message.answer(("Пришли мне свой лицевой"))
    await state.set_state(("add_credit"))


@dp.message_handler(state="add_credit")
async def enter_credit(message: types.Message, state: FSMContext):
    c = message.text
    if c.isdigit() and len(c) <= 6 and c!='0':
        l = await commands.add_owner(user_id=message.from_user.id, contract_id=int(message.text))
        if l == -3:
            await message.answer("Такой лицевой уже есть", reply_markup=main_menu)
        if l == 0:
            await message.answer("Лицевой добавлен", reply_markup=main_menu)
        else:
            await message.answer('Лицевой не найден')
    else:
        await message.answer("Не существует такого лицевого")

    await state.finish()
    await message.answer("Продолжите работу или вернитесь назад", reply_markup=contract_menu)


@dp.message_handler(text=("Назад ⬅"))
async def get_credit(message: types.Message):
    await message.answer("Выберите услугу в меню", reply_markup=main_menu)
