from aiogram import executor
from utils.set_bot_commands import set_default_commands
from loader import db
from utils.db_api import db_gino
from loader import dp
import middlewares, filters, handlers


async def on_startup(dp):
    from utils.notify_admins import on_startup_notify

    print("Подключаем БД")
    await  db_gino.on_startup(dp)
    print("Готово")


    # Устанавливаем дефолтные команды
    await set_default_commands(dp)

    # Уведомляет про запуск
    await on_startup_notify(dp)

if __name__ == '__main__':
    executor.start_polling(dp, on_startup=on_startup)
