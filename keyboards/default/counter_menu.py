from aiogram.types import ReplyKeyboardMarkup, KeyboardButton

counter_menu = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text='Просмотр информации о счетчиках')
        ],
        [
            KeyboardButton(text='Последние показания счетчиков'),
        ],

        [
            KeyboardButton(text='Назад ⬅'),
        ]
    ],
    resize_keyboard=True
)
