from aiogram.types import ReplyKeyboardMarkup, KeyboardButton

contract_menu = ReplyKeyboardMarkup(
    keyboard=[
        [
            KeyboardButton(text='Просмотр лицевых счетов')
        ],
        [
            KeyboardButton(text='Удалить лицевой счет'),
        ],
        [
            KeyboardButton(text='Добавить лицевой счет'),
        ],
        [
            KeyboardButton(text='Назад ⬅'),
        ]
    ],
    resize_keyboard=True
)
