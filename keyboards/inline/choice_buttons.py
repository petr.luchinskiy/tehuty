from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton
from keyboards.inline.callback_datas import buy_callback
choice = InlineKeyboardMarkup(row_width=4, inline_keyboard=[
    [
        InlineKeyboardButton(
            text='Начисление',
            callback_data='rkc:credit'
        ),
        InlineKeyboardButton(
            text='Госповерка',
            callback_data='rkc:counters'
        )],
[
    InlineKeyboardButton(
        text='Указать телефон',
        callback_data='rkc:phone'
    ),
    InlineKeyboardButton(
       text='Передать показания',
       callback_data='rkc:counter_reading'
    )
],
[
    InlineKeyboardButton(
        text='Отмена',
        callback_data='cancel'
    )
]
])

pear_keyboard = InlineKeyboardMarkup()

PEAR_LINK = 'rudvodokanal.kz'

pear_link = InlineKeyboardButton(text='официальный сайт', url=PEAR_LINK)
pear_keyboard.insert(pear_link)