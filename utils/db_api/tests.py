import asyncio

from data import config
from utils.db_api import quick_commands
from utils.db_api.db_gino import db


async  def test():
    await  db.set_bind(config.POSTGRES_URI)
    await  db.gino.drop_all()
    await  db.gino.create_all()

    print('Добавляем пользователей')
    await quick_commands.add_user(1, 'one', 'vasya@mail.ru')
    await quick_commands.add_user(2, 'two', 'vasya2@mail.ru')
    await quick_commands.add_user(3, 'three', 'vasya3@mail.ru')
    await quick_commands.add_user(4, 'four', 'vasya4@mail.ru')
    print('done')
    print(f'получаем пользователей')
    users = await  quick_commands.select_all_users()

    count_users = await quick_commands.count_users()
    print(f'всего пользователей: {count_users}')

    user = await  quick_commands.select_user(id=4)
    print(f"Получил пользователя:{user}")



loop = asyncio.get_event_loop()
loop.run_until_complete(test())