from sqlalchemy import Column, BIGINT, String, sql, Date

from utils.db_api.db_gino import BaseModel


class Record_counters(BaseModel):
    __tablename__ = 'record_counters'
    contract_id = Column(BIGINT)
    counter_id = Column(BIGINT, primary_key=True)
    type_counter = Column(String(200))
    place = Column(String(200))
    date_record = Column(Date)
    value = Column(String(200))
    actual = Column(Date)



    query: sql.Select
