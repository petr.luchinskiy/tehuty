from sqlalchemy import Column, BIGINT, String, sql

from utils.db_api.db_gino import BaseModel


class Owner(BaseModel):
    __tablename__ = 'owner'
    user_id = Column(BIGINT)
    contract_id = Column(BIGINT,  primary_key=True)
    address = Column(String(200))

    query: sql.Select
