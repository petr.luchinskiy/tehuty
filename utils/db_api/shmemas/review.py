from sqlalchemy import Column, BIGINT, String, sql, Text

from utils.db_api.db_gino import BaseModel



class Review(BaseModel):
    __tablename__ = 'reviews'
    id_review = Column(BIGINT,  primary_key=True)
    id_user = Column(BIGINT)
    text_review = Column(Text)


    query: sql.Select
