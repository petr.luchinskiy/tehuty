from sqlalchemy import Column, BIGINT, String, sql, Date

from utils.db_api.db_gino import BaseModel


class Counters(BaseModel):
    __tablename__ = 'counters'
    contract_id = Column(BIGINT, primary_key=True)
    counter_id = Column(BIGINT)
    type_counters = Column(String(200))
    factory_numbers = Column(String(200))
    next_verification_date = Column(Date)
    place = Column(String(200))
    date_accept = Column(Date)
    actual = Column(String(200))



    query: sql.Select
