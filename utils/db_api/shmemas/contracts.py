
from sqlalchemy import Column, BIGINT, String, sql

from utils.db_api.db_gino import BaseModel

class Contracts(BaseModel):
    __tablename__='contracts'
    contract_id = Column(BIGINT, primary_key=True)
    street = Column(String(100))
    house = Column(String(100))
    flat = Column(String(100))
    fam = Column(String(100))
    name = Column(String(100))
    patronomic= Column(String(100))
    hvstek = Column(String(100))
    polivtek = Column(String(100))
    penyatek= Column(String(100))
    actual = Column(String(200))

    query: sql.Select