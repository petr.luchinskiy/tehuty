
from sqlalchemy import Column, BIGINT, String, sql

from utils.db_api.db_gino import BaseModel

class User(BaseModel):
    __tablename__='users'
    user_id = Column(BIGINT, primary_key=True)
    name = Column(String(100))
    phone = Column(String(100))


    query: sql.Select