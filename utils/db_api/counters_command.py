from aiogram.types import message
from asyncpg import UniqueViolationError
import types
from utils.db_api.db_gino import db
from utils.db_api.shmemas.contracts import Contracts
from utils.db_api.shmemas.counters import Counters
from utils.db_api.shmemas.record_counters import Record_counters
from aiogram import types

from utils.db_api.shmemas.contracts import Contracts


async def view_counters_detail(contract_id):
    rec = await Counters.query.where(Counters.contract_id == contract_id).gino.all()
    if rec != []:
        return rec
    else:
        return -3

async def view_records_detail(contract_id: int):
    rec = await Record_counters.query.where(Record_counters.contract_id == contract_id).gino.all()
    if rec != []:
        return rec
    else:
        return -3
