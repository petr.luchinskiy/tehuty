from asyncpg import UniqueViolationError
from sqlalchemy import Column, DateTime

from utils.db_api.db_gino import db
from utils.db_api.shmemas import user
from utils.db_api.shmemas.user import User
from utils.db_api.shmemas.review import Review
from utils.db_api.shmemas.contracts import Contracts


async def add_user(user_id: int, name: str, phone: str = None):
    try:
        user = User(user_id=user_id, name=name, phone=phone)
        await  user.create()
    except UniqueViolationError:
        pass


async def select_all_users():
    users = await User.query.gino.all()
    return users


async def select_user(user_id: int):
    user = await User.query.where(User.user_id == user_id).gino.first()
    return user


async def count_users():
    total = await db.func.count(User.user_id).gino.scalar()
    return total


async def count_debet():
    total = await db.func.count(Debet.dogovorid).gino.scalar()
    return total

async def select_licevoy(id: int):
    licevoy = await Debet.query.where(Debet.dogovorid == id).gino.first()
    return licevoy

async def update_user_phone(user_id, phone):
    user = await  User.get(user_id)
    await user.update(phone=phone).apply()


async def update_user_email(id, email):
    user = await  User.get(id)
    await user.update(email=email).apply()

async def add_review(user_id: int, text_review: str):
    try:
        date = Column(DateTime(True), server_default=db.func.now())
        review = Review(id_user=user_id, text_review=text_review, date_review=date)
        await  review.create()
    except UniqueViolationError:
        pass