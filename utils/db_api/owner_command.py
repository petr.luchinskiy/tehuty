from aiogram.types import message
from asyncpg import UniqueViolationError
import types
from utils.db_api.db_gino import db
from utils.db_api.shmemas.owner import Owner
from aiogram import types

from utils.db_api.shmemas.contracts import Contracts as q
from utils.db_api.shmemas.contracts import Contracts


async def view_credit_detail(contract_id: int):
    credit = await q.query.where(q.contract_id == contract_id).gino.first()
    if credit is not None:
        return credit
    else:
        return -3

async def add_owner(user_id: int, contract_id: int):
    try:
        checkcontract = await Owner.query.where(Owner.contract_id == contract_id).gino.first()
        checkuser = await Owner.query.where(Owner.user_id == user_id).gino.first()
        if checkcontract is None or checkuser is None:
            contract = await Contracts.query.where(Contracts.contract_id == contract_id).gino.first()
            if contract is None:
                return contract
            else:
                if contract.flat is None:
                    addressl = contract.street.strip() + ' дом ' + contract.house.strip()
                    owner = Owner(user_id=user_id, contract_id=contract_id, address=addressl)
                    await  owner.create()
                    return 0
                else:
                    addressl = contract.street.strip() + ' дом ' + contract.house.strip() + ' кв ' + contract.flat.strip()
                    owner = Owner(user_id=user_id, contract_id=contract_id, address=addressl)
                    await  owner.create()
                    return 0
        else:
            return -3
    except UniqueViolationError:
        pass


async def del_owner(contract_id: int):
    owners = await Owner.query.where(Owner.contract_id == contract_id).gino.first()
    if owners is not None:
        owner = Owner(contract_id=contract_id)
        await  owner.delete()
        return 0
    else:
        return -3


async def select_all_owners(user_id: int = 0):
    try:
        owners = await Owner.query.where(Owner.user_id == user_id).gino.all()
        return owners
    except UniqueViolationError:
        pass


async def select_owner(id: int):
    licevoy = await Contracts.query.where(Owner == id).gino.first()
    return licevoy
#
#
#
# async def select_user(user_id: int):
#     user = await User.query.where(User.user_id == user_id).gino.first()
#     return user
#
#
# async def count_users():
#     total = await db.func.count(User.user_id).gino.scalar()
#     return total
#
#
# async def count_debet():
#     total = await db.func.count(Debet.dogovorid).gino.scalar()
#     return total
#
#
# async def select_licevoy(id: int):
#     licevoy = await Debet.query.where(Debet.dogovorid == id).gino.first()
#     return licevoy
#
#
# async def update_user_phone(user_id, phone):
#     user = await  User.get(user_id)
#     await user.update(phone=phone).apply()
#
#
# async def update_user_email(id, email):
#     user = await  User.get(id)
#     await user.update(email=email).apply()
