import os
from environs import Env
from dotenv import load_dotenv


# Теперь используем вместо библиотеки python-dotenv библиотеку environs
env = Env()
env.read_env()

BOT_TOKEN = env.str("BOT_TOKEN")  # Забираем значение типа str
PGUSER=str(os.getenv("PGUSER"))
PGPASSWORD=str(os.getenv("PGPASSWORD"))
DATABASE=str(os.getenv("DATABASE"))

IP = env.str("ip")  # Тоже str, но для айпи адреса хоста

POSTGRES_URI=f'postgresql://{PGUSER}:{PGPASSWORD}@{IP}/{DATABASE}'


load_dotenv()


ADMINS = [
    498127977
    #1250403236 talikova
]

ip = os.getenv("ip")

aiogram_redis = {
    'host': ip,
}

redis = {
    'address': (ip, 6379),
    'encoding': 'utf8'
}

